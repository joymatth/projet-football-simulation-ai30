package game

import (
	"fmt"
	t "football-manager/agent"
)

func NewMatchDay(dayNum int, rankingInit map[string]int) *t.MatchDay{
	var games []*t.Game

	return &t.MatchDay{DayNum: dayNum, Games: games, CurrentRanking: rankingInit}
}

func InitRanking(teams []*t.Team) map[string]int {
	rankingInit := make(map[string]int)
	for _, team := range teams {
		rankingInit[team.ClubName] = 0
	}

	return rankingInit
}

/*
func RecalculDayRanking(mDay *t.MatchDay) {
	fmt.Println(mDay.DayNum)
	for _, game := range mDay.Games {
		fmt.Println(game.HomeTeam.ClubName, " : ", game.HomeTeam.Points, game.AwayTeam.ClubName, " : ", game.AwayTeam.Points)
		mDay.CurrentRanking[game.HomeTeam.ClubName] = game.HomeTeam.Points
		mDay.CurrentRanking[game.AwayTeam.ClubName] = game.AwayTeam.Points
	}	
}*/

func PrintRanking(ranking map[string]int) {
	for team, points := range ranking {
		fmt.Println(team, " : ", points) 
	}
}

func GetMatchDay(dayNum int, calendar []*t.MatchDay, rankingInit map[string]int) ([]*t.MatchDay, *t.MatchDay) {
	for _, mDay := range calendar {
		if mDay.DayNum == dayNum {
			return calendar, mDay
		} 
	}

	newMatchDay := NewMatchDay(dayNum, rankingInit)
	calendar = append(calendar, newMatchDay)
	return calendar, newMatchDay
}

// renvoie True si l'équipe ne joue pas au jour indiqué, false sinon
func DoesntPlayToday(day *t.MatchDay, team *t.Team) bool{
	gamesOfTheDay := day.Games
	for i:= range gamesOfTheDay{
		if gamesOfTheDay[i].AwayTeam.Id == team.Id || gamesOfTheDay[i].HomeTeam.Id == team.Id {
			return false
		}
	}
	return true
}

func isMatchDayFull(mDay *t.MatchDay, nMatch int) bool {
	return len(mDay.Games) == nMatch
}

func Positive(x int) int {
	if x > 0 {
		return x
	} else {
		return 0
	}
}

func CreateMatchDay(dayNum int, league *t.League, rankingInit map[string]int) []*t.MatchDay {
	var mDay *t.MatchDay
	league.Calendar, mDay = GetMatchDay(dayNum, league.Calendar, rankingInit)
	teams := league.Teams
	nTeam := len(teams)
	
	nMatch := (nTeam / 2)
	nMatchAdd := 0

	i := dayNum
	y := 0

	for z := 0; z < nMatch - Positive(dayNum - nMatch); z++ {
		
		//x := int(math.Abs(float64((nTeam - i - dayNum + 1))))
		x := nTeam - i
		if x < y {
			i = 1
			x = nTeam - i
			y += z - 1
		}

		if (dayNum > nMatch && y != 0 ) {
			if y == 1 {
				i = 0
			}
			x = nTeam - 1 - 2*(dayNum - nMatch) - i
		}
		if teams[y].ClubName != teams[x].ClubName {
			matchId := fmt.Sprintf("%d-%d", dayNum, len(mDay.Games)+1)
			mDay.Games = append(mDay.Games, NewGame(matchId, teams[y], teams[x]))
			nMatchAdd ++
		}
		
		if y != 0 || dayNum == 1 {
			i ++
		} else {
			i += dayNum
		}

		y++
	}

	diffMatch := nMatch - nMatchAdd
	if diffMatch > 0 {
		i = 1
		for a := nTeam - diffMatch*2; nMatchAdd != nMatch; a++ {
			x := nTeam - i
			if teams[a].ClubName != teams[x].ClubName {
				matchId := fmt.Sprintf("%d-%d", dayNum, len(mDay.Games)+1)
				mDay.Games = append(mDay.Games, NewGame(matchId, teams[a], teams[x]))
				nMatchAdd ++
			}
			i++
		}
	}

	return league.Calendar
}

func CreateInverseMatchDay(mDay *t.MatchDay, league *t.League, rankingInit map[string]int) []*t.MatchDay{
	newMDayNum := mDay.DayNum + len(league.Teams) - 1

	var newMDay *t.MatchDay
	league.Calendar, newMDay = GetMatchDay(newMDayNum, league.Calendar, rankingInit)

	for _, game := range mDay.Games {
		matchId := fmt.Sprintf("%d-%d", newMDayNum, len(newMDay.Games)+1)
		newMDay.Games = append(newMDay.Games, NewGame(matchId, game.AwayTeam, game.HomeTeam))
	}

	return league.Calendar
}

func SimulMatchDay(matchDay *t.MatchDay) {
	for _, game := range matchDay.Games {
		PlayGame(game)
		matchDay.CurrentRanking[game.HomeTeam.ClubName] = game.HomeTeam.Points
		matchDay.CurrentRanking[game.AwayTeam.ClubName] = game.AwayTeam.Points
	}
	//RecalculDayRanking(matchDay)
}

func PrintMatchDay(matchDay *t.MatchDay) {
	fmt.Printf("Day n°%d\n", matchDay.DayNum)
	for _, game := range matchDay.Games {
		fmt.Printf("Game %s : %s VS %s\n", game.Id, game.HomeTeam.ClubName, game.AwayTeam.ClubName)
		fmt.Printf("Resultat : %d - %d  ====> Winner %s\n", game.HomeScore, game.AwayScore, game.Result)
	}
}