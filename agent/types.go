package agent

type Player struct {
	Id           int          `json:"Id"`
	Name         string       `json:"Nom"`
	Country      string       `json:"Pays"`
	Overall      int          `json:"Général"`
	Club         string       `json:"Club"`
	Passing      int          `json:"Passes courtes"`
	Shooting     int          `json:"Puissance frappe"`
	Agressivity  int          `json:"Agressivite"`
	Stamina      int          `json:"Endurance"`
	Strength     int          `json:"Force"`
	Skills       int          `json:"Dribbles"`
	Finishing    int          `json:"Finition"`
	Volee        int          `json:"Volee"`
	DefConsience int          `json:"Conscience defensive"`
	Interception int          `json:"Interception"`
	TacleDebout  int          `json:"Tacle debout"`
	TacleGlisse  int          `json:"Tacle glisse"`
	Reflexe      int          `json:"Reflexes"`
	Plongeon     int          `json:"Plongeon"`
	JeuMain      int          `json:"jeu main"`
	JeuPied      int          `json:"Jeu pied"`
	Offensif     string       `json:"Rendement offensif"`
	Defensif     string       `json:"Rendement defensif"`
	Penalty      int          `json:"Penalty"`
	Vista        int          `json:"Vista"`
	Position     string       `json:"Position"`
	Goal         int          `json:"Goal"`
	YellowCards  int          `json:"YellowCards"`
	RedCards     int          `json:"RedCards"`
	ChannelRec   chan float64 `json:"-"`
}

type Trainer struct {
	Id          int         `json:"id"`
	Name        string      `json:"name"`
	Team        string      `json:"club"`
	Strategy    string      `json:"strategy"`
	TrainerChan chan string `json:"-"`
}

type Team struct {
	Id          int         `json:"id"`
	ClubName    string      `json:"name"`
	Code        string      `json:"code"`
	Overall     int         `json:"overall"`
	Strategy    string      `json:"strategy"`
	Country     string      `json:"country"`
	TeamLeague  *League     `json:"-"`
	Players     []*Player   `json:"-"`
	TrainerName string      `json:"TrainerName"`
	Points      int         `json:"points"`
	TrainerChan chan string `json:"-"`
	Titulars    []*Player   `json:"-"`
	Subsitutes  []*Player   `json:"-"`
}

type TeamJSON struct {
	Id          int       `json:"id"`
	ClubName    string    `json:"name"`
	Code        string    `json:"code"`
	Overall     int       `json:"overall"`
	Strategy    string    `json:"strategy"`
	Country     string    `json:"country"`
	TeamLeague  *League   `json:"league"`
	Players     []*Player `json:"players"`
	TrainerName string    `json:"TrainerName"`
	Points      int       `json:"points"`
}

type League struct {
	Id       int         `json:"id"`
	Name     string      `json:"name"`
	Country  string      `json:"country"`
	Teams    []*Team     `json:"teams"`
	Calendar []*MatchDay `json:"calendar"`
}

type MatchDay struct {
	DayNum         int     `json:"dayNum"`
	Games          []*Game `json:"games"`
	CurrentRanking map[string]int
}

type Game struct {
	Id              string    `json:"id"`
	HomeTeam        *Team     `json:"homeTeam"`
	AwayTeam        *Team     `json:"awayTeam"`
	Result          string    `json:"result"`
	HomeScore       int       `json:"homeScore"`
	HomeYellowCards int       `json:"homeYellowCards"`
	HomeRedCards    int       `json:"homeRedCards"`
	HomeLogs        string    `json:"homeLogs"`
	AwayScore       int       `json:"awayScore"`
	AwayYellowCards int       `json:"awayYellowCards"`
	AwayRedCards    int       `json:"awayRedCards"`
	AwayLogs        string    `json:"awayLogs"`
	RandTab         []float64 `json:"-"`
	NAction         int       `json:"-"`
}

type LeagueIdRequest struct {
	LeagueId int `json:"LeagueId"`
}

type AllPlayersResponse struct {
	Players []Player `json:"Players"`
}

type PlayerInfoRequest struct {
	PlayerId int `json:"LeagueId"`
}

/*
type PlayerInfoResponse struct {
	PlayerInfo PlayerInfo `json:"PlayerInfo"`
}*/

type AllLeaguesResponse struct {
	Leagues []*League `json:"Leagues"`
}

type GetCalendarResponse struct {
	Calendar map[int][]*Game `json:"calendar"`
}

type GetAllClassementsResponse struct {
	Classements map[int]map[string]int `json:"classements"`
}

type PlayerRequest struct {
	Id          int    `json:"ID"`
	Name        string `json:"Name"`
	Age         int    `json:"Age"`
	Photo       string `json:"Photo"`
	Nationality string `json:"Nationality"`
	Flag        string `json:"Flag"`
	Overall     int    `json:"Overall"`
	Club        string `json:"Club"`
	ClubLogo    string `json:"Club Logo"`
	Position    string `json:"Position"`
}
