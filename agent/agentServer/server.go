package agentServer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	t "football-manager/agent"
	g "football-manager/agent/game"
	l "football-manager/agent/league"
)

type RestServerAgent struct {
	sync.Mutex
	id       string
	reqCount int // Ce n'est pas du REST car il garde une trace
	addr     string
	Leagues  []*t.League
	Teams    []*t.Team
	Players  []*t.Player
	Trainers []*t.Trainer
}

func NewRestServerAgent(addr string, leagues []*t.League, teams []*t.Team, players []*t.Player, trainers []*t.Trainer) *RestServerAgent {
	return &RestServerAgent{id: "Serveur unique", addr: addr, Leagues: leagues, Teams: teams, Players: players, Trainers: trainers} 
}

func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

/*
func (*RestServerAgent) decodeRequest(r *http.Request) (req, err) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	return req, err
}
*/

func (rsa *RestServerAgent) sendAllPlayers(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.LeagueIdRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	var resp t.AllPlayersResponse

	league := l.GetLeague(rsa.Leagues, req.LeagueId)

	players := l.GetLeaguePlayers(league)

	resp.Players = players

	fmt.Println("nb Joueurs : ", len(resp.Players))

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}


func (rsa *RestServerAgent) sendLeagueCalendar(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.LeagueIdRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	var resp t.GetCalendarResponse

	league := l.GetLeague(rsa.Leagues, req.LeagueId)

	resp.Calendar = make(map[int][]*t.Game)

	for i, mDay := range league.Calendar {
		if i == 0 {
			resp.Calendar[mDay.DayNum] = append(resp.Calendar[mDay.DayNum], mDay.Games...)
		}
	}

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) sendSimulation(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.LeagueIdRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	league := l.GetLeague(rsa.Leagues, req.LeagueId)

	l.ResetSimul(league)

	fmt.Println("Debut simulation", league.Name)
	for _, mDay := range league.Calendar {
		g.SimulMatchDay(mDay)
	}
	fmt.Println("Fin simulation classement final : ", league.Name)

	g.PrintRanking(league.Calendar[len(league.Calendar)-1].CurrentRanking)
	// traitement de la requête
	var resp t.GetCalendarResponse

	resp.Calendar = make(map[int][]*t.Game)

	for _, mDay := range league.Calendar {
		resp.Calendar[mDay.DayNum] = append(resp.Calendar[mDay.DayNum], mDay.Games...)
	}
	/*
	for i, mDay := range resp.Calendar {
		fmt.Println(i)
		for _, game := range mDay {
			fmt.Printf("%s %s\n", game.HomeTeam.ClubName, game.AwayTeam.ClubName)
		}
	}*/

	//l.PrintLeague(league)

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) sendAllClassements(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.LeagueIdRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	league := l.GetLeague(rsa.Leagues, req.LeagueId)
	
	// traitement de la requête
	var resp t.GetAllClassementsResponse

	resp.Classements = make(map[int]map[string]int)

	for _, mDay := range league.Calendar {
		//fmt.Println(mDay.DayNum)
		//g.PrintRanking(mDay.CurrentRanking)
		resp.Classements[mDay.DayNum] = mDay.CurrentRanking
	}
	/*
	for i, mDay := range resp.Calendar {
		fmt.Println(i)
		for _, game := range mDay {
			fmt.Printf("%s %s\n", game.HomeTeam.ClubName, game.AwayTeam.ClubName)
		}
	}*/

	//l.PrintLeague(league)

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)

	l.ResetLeagueGames(league)
}

/*
func (rsa *RestServerAgent) sendPlayerStat(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.PlayerInfoRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	var resp t.Player

	resp = *p.GetPlayers(req.PlayerId, rsa.Players)

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) sendTeams(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.AllPlayersRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête

	var resp t.AllPlayersResponse

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) sendTeamStat(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.AllPlayersRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête

	var resp t.AllPlayersResponse

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) sendMatch(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	var req t.AllPlayersRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête

	var resp t.AllPlayersResponse

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}*/


func (rsa *RestServerAgent) sendAllLeagues(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}
	/*
	// décodage de la requête
	var req t.AllPlayersRequest
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err := json.Unmarshal(buf.Bytes(), &req)
	*/
	/*
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}*/

	// traitement de la requête
	var resp t.AllLeaguesResponse

	resp.Leagues = rsa.Leagues

	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/get_all_players", rsa.sendAllPlayers)
	//mux.HandleFunc("/get_player_info", rsa.sendPlayerStat)
	//mux.HandleFunc("/get_teams", rsa.sendTeams)
	//mux.HandleFunc("/get_team_stat", rsa.sendTeamStat)
	//mux.HandleFunc("/get_match", rsa.sendMatch)
	mux.HandleFunc("/get_all_leagues", rsa.sendAllLeagues)
	mux.HandleFunc("/get_calendar", rsa.sendLeagueCalendar)
	mux.HandleFunc("/run_simulation", rsa.sendSimulation)
	mux.HandleFunc("/get_all_classements", rsa.sendAllClassements)
	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
