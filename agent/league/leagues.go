package league

import (
	"encoding/json"
	"fmt"
	t "football-manager/agent"

	g "football-manager/agent/game"
	"io/ioutil"
)

type LeagueJSON struct {
	Id int
	Name  string    `json:"name"`
	Teams []TeamJSON `json:"clubs"`
}

type TeamJSON struct {
	Name string `json:"name"`
	Code        string `json:"code"`
	CountryName string `json:"country"`
}

func CreateLeagues(teams []*t.Team) (leagues []*t.League) {
	// ouverture du fichier
	content, err := ioutil.ReadFile("bdd_leagues.json")
	if err != nil {
		fmt.Println(err.Error())
	}

	// unmarshal
	var leaguesJSON []*LeagueJSON
	json.Unmarshal(content, &leaguesJSON)
	
	var newLeagues []*t.League
	for i, league := range leaguesJSON {
		league.Id = i+1
		newLeagues = append(newLeagues, NewLeague(i+1, league.Name, league.Teams[0].CountryName))
	}

	FillLeagues(leaguesJSON, newLeagues, teams)
	newLeagues = verifLeagues(newLeagues)

	for _, league  := range newLeagues {
		fmt.Printf("\n================== Championnat : %s (ID=%d) (Nombre d'équipes : %d)==================\n", league.Name, league.Id, len(league.Teams))
	}
	return newLeagues
}

func NewLeague(id int, name string, country string) *t.League {
	var teams []*t.Team
	return &t.League{Id: id, Name: name, Country: country, Teams: teams}
}

func verifLeagues(leagues []*t.League) []*t.League{
	nLeague := len(leagues)
	for i := 0; i < nLeague; i++{
		if len(leagues[i].Teams) < 10 || len(leagues[i].Teams)%2 != 0 {
			fmt.Println("ici : ", leagues[i].Name, " nteam : ", len(leagues[i].Teams), " n%2", len(leagues[i].Teams)%2)
			leagues = append(leagues[:i], leagues[i+1:]...)
			nLeague--
			i--
		}
	}

	return leagues
}

/*
	func leagueOfTheTeam(team *t.Team) {
		leagues := LeaguesJSON()
		for i := range leagues {
			for j := range leagues[i].Teams {
				if team.ClubName == leagues[i].Teams[j].ClubName {
					team.League = leagues[i].Name
				}
			}
		}
	}
*/

func GetLeague(leagues []*t.League, id int) *t.League {
	for _, l := range leagues {
		if l.Id == id {
			return l
		}
	}

	fmt.Println("pas de league")
	return nil
}

func GetId(l *t.League) int {
	return l.Id
}

func GetLeaguePlayers(l *t.League) (players []t.Player) {

	for _, team := range l.Teams {
		for _, pla := range team.Players {
			players = append(players, *pla)
		}
	}

	return players
}

func FillLeagues(leaguesJSON []*LeagueJSON, leagues []*t.League, teams []*t.Team) {
	for _, leagueJSON := range leaguesJSON {
		for _, team := range teams {
			n := 1
			for _, team2 := range leagueJSON.Teams {
				if team.ClubName == team2.Name {
						lTemp := GetLeague(leagues, leagueJSON.Id)
						lTemp.Teams = append(lTemp.Teams, team)
						team.TeamLeague = lTemp
						n ++ 
				}
			}
		}
	}
}

func CreateCalendar(league *t.League) *t.League{
	teams := league.Teams

	nTeam := len(teams)
	nMatchDay := (nTeam - 1)

	for dayNum := 1; dayNum <= nMatchDay; dayNum++{
		league.Calendar = g.CreateMatchDay(dayNum, league, g.InitRanking(teams))
	}

	for dayNum := nMatchDay + 1; dayNum <= nMatchDay*2 ; dayNum++{
		_, oldMDay := g.GetMatchDay(dayNum - nMatchDay, league.Calendar, g.InitRanking(teams))
		league.Calendar = g.CreateInverseMatchDay(oldMDay, league, g.InitRanking(teams))
	}

	return league
}

func ResetSimul(league *t.League) {
	for _, team := range league.Teams {
		team.Points = 0
	} 
}

func PrintLeague(league *t.League) {
	fmt.Printf("\n================== Championnat : %s (ID=%d) (Nombre d'équipes : %d)==================\n", league.Name, league.Id, len(league.Teams))
	for _, mDay := range league.Calendar {
		g.PrintMatchDay(mDay)
	}
	g.PrintRanking(league.Calendar[len(league.Calendar)-1].CurrentRanking)		
}

func ResetLeagueGames(league *t.League) {
	for _, mDay := range league.Calendar {
		for _, game := range mDay.Games {
			game.HomeScore = 0
			game.AwayScore = 0 
		}
	}

}