# Football Manager 2023

Ce projet a pour but de simuler des matchs de Football entre différentes équipes au sein de différentes ligues. 
La particularité de ce projet est que l'interface graphique est compatible avec la Nintendo Switch.  
Notre projet tente de répondre a la problèmatique suivante :  
**Est-ce que les resultats des ligues de football peuvent être prevus d'après les statistiques de fifa 23 ?**

## Présentation du projet

Le projet peut-être découpé en 4 grandes parties : 
- l'initialisation des différents agents
- la simulation des ligues
- l'envoi des résultats sous forme de requêtes
- création de l'interface switch.

### Initialisation des différents agents

Tout est considérés comme agent : Les joueurs, les équipes, les ligues...

Les **joueurs** ont été récupérés depuis la base de données officielle des joueurs de FIFA 23 sous format JSON. On récupère ainsi les informations 'classiques' du joueur : son Nom, son Club, son Pays... mais également une liste d'attributs qui nous permettront de faire des simulations réalistes (Réflexes, Finition, Agressivité, Volée..). Ces attributs sont des entiers compris entre 0 et 100, plus l'indicateur est élevé plus le joueur est bon dans le domaine.

Les **entraîneurs**. De même que pour les ligues, il est assez difficile de trouver des données sur Internet sur les entraîneurs. On a donc décidé de créer un JSON manuellement, mais uniquement pour la Ligue 1.
Chaque entraîneur est associé à un Club et a une stratégie qui lui est propre (4-4-2, 4-3-3 ou 3-5-2).  
La **communication inter-agent** entre a ce niveau là, l'entraineur va communiquer à son équipe sa stratégie via un channel à l'initialisation celle-ci.

Les **équipes** sont créées à partir des joueurs récupérés ci-dessus. En effet, on a préféré partir des joueurs pour créer nos équipes, plutôt que de se baser sur une base de données d'équipes. L'idée est de n'avoir que des équipes ayant des joueurs dans notre base. Une équipe a une **ligue**, une liste de **joueurs** titulaires, une liste de **joueurs** remplaçant et un **entraîneur**. Les **joueurs** titulaires sont les meilleures joueurs à chaque poste en fonction de la stratégie de l'équipe, les remplacants sont le reste de l'équipe. Chaque équipe a un score général calculé par la moyenne de la somme des scores des titulaires plus la somme des générales des remplaçants

Les **ligues** sont, à l'instar des joueurs, récupérés à partir d'une fichier JSON trouvé sur Internet.
Une ligue a donc un nom (Ligue 1, Liga, Serie A...), un pays, une liste d'**équipes** ainsi qu'un calendrier (qui planifie les journées et les matchs de chaque équipes). Seules les ligues pouvant etre simulées sont gardées, c'est à dire avec un nombre d'équipes pair et supérieur à 10 (la plupart de ces ligues sont complètes). 

*Remarque* : concernant le JSON utilisé pour nos ligues, il s'agit de données datant de la saison 2020/2021. Nous n'avons pas réussi à trouver de données plus récentes. Depuis, certaines équipes ont été reléguées (ou promues), il est donc normal qu'il y ait quelques incohérences avec la réalité actuelle. 

Nous avons également les **matchs**. Un match est une rencontre de deux **équipes**, avec un ensemble d'informations concernant le déroulé du match (le résultat final, le nombre de cartons jaunes, de cartons rouges...)

Plusieurs matchs ont lieu au sein d'une même **journée**. Une journée est une liste de **matchs**. Chaque équipe doit faire un match aller et retour contre toutes les autres équipes du championnat (une fois à domicile, et une autre à l'extérieur). Une équipe ne peut pas jouer deux fois dans la même journée.
L'ensemble des journées constitueront donc le calendrier d'une ligue. Pour créer ce calendrier nous avons utilisé l'algorithme de création de tournoi [Round-Robin](https://en.wikipedia.org/wiki/Round-robin_tournament#Scheduling_algorithm). Pour n équipes il y aura alors n-1 matchs aller et n-1 matchs retour. 

### Simulation des ligues

Les ligues se déroulent suivant leur **calendrier**, pour les simuler on va donc parcourir les **journées** de ce calendrier dans
leur ordre de création. Pour chaque journée nous simulons tous les **matchs** en stockants les resultats et les logs des actions.

La simulation d'un **match** se décompose en plusieurs étapes :
1. On détermine le nombre d'actions qu'obtiendront les **joueurs** titulaires durant le **match**, durant cette étape on obtient le nombre de tir, de potentiel carton et carton rouge pour chacun d'eux. Le nombre d'action d'un **joueur** est determiné par plusieurs critéres :
- La position de l'**équipe** du joueurs, Domicile ou Extérieur
- La différence de général entre les 2 **équipes**
- Le rendement Offensif et la position du **joueur** pour les tirs
- Le rendement defensif et l'aggresivité pour les carton Jaune et Rouge

2. On parcourt les 2 **équipes** et on simule chaque action le nombre de fois determiner par l'étape 1 pour chaque **joueurs**. La simulation des actions s'effectue avec les statistiques du **joueur**, on determine un score en utilisant : 
- Le tir, la finition et les dribbles pour le score de tir
- Les reflexes, les plongeon, le jeu au pied et je le jeu a la main pour le score d'arret d'un gardien
- Les tacles debouts et tacles glissées pour le carton Jaune  
Ensuite on transmet ce score par channel et on le compare avec un float random pour determiner la réussite de l'action. Pour les tirs, si il est réussi il n'y a pas forcément but. En effet, on compare ensuite le score obtenu par le tireur avec le score du gardien (on ajoute un chiffre random des 2 cotés pour rendre la comparaison aléatoire), si le score du gardien est meilleur, celui-ci arrete le tir et il n'y a donc pas but.

3. On détermine le gagnant et mets a jours les différentes informations. Pour chaque tir réussi et non arrété on ajoute 1 but à l'**équipe** du tireur. Si une **équipe** est gagnante on lui ajoute 3 points au classement et si c'est une égalité on ajoute 1 point aux 2 **équipes**.  

Chaque **journée** de la **ligue** enregistre le classement des équipes à cette journée. Cela permet de consulter les classements après chacune des **journées** et pas seulement le classement final.

*Remarque* : Une fois la simulation de ligue completement terminé on reset les points pour permettre de la re-simuler.

### Envoi des requêtes & Interface 

La communication serveur client s'effectue via des requetes REST. Voici les différentes requetes utilisables sur notre serveur :
- */get_all_players*     : permet au client de recuperer un JSON contenant tout les players et leurs informations.
- */get_all_leagues*     : permet au client de recuperer un JSON contenant toutes les ligues et leurs informations (donc aussi toutes les équipes).
- */get_calendar*        : permet de recuperer un JSON contenant le calendrier de match d'une ligue spécifique.
- */run_simulation*      : permet de lancer la simulation d'une ligue et renvoi le calendrier de cette ligue avec tout les matchs joués.
- */get_all_classements* : permet d'obtenir tous les classement d'une league, c'est à dire le classement de toutes les journées.

*remarque* : les ids de ligue sont communiqués dans la requete envoyé par le client au serveur

### Création de l'interface Switch

Le front end a été réalisé avec la librarie LVGL, connu pour son utilisation dans le domaine de l'informatique embarqué. La librarie a été associé aux drivers SDL2 fourni par les développeurs.  
  
Ce front-end est cross compatible Linux/HorizonOS et cross compilable avec la toolchain indépendante DevKitArm, ainsi que la HAL (Hardware Abstraction Layer) LibNX.  
  
La communication réseau a été réalisé avec la librarie C LibCurl connu pour sa stabilité, ses performances ainsi que sa compatibilité.

Les informations étant encodé en JSON, nous avons utilisé la librarie JSON-C pour décoder la chose de façon optimisé et performante.

La génération du projet a été développée sous Cmake, pour permettre une grande modulabilité sur les différentes plateformes.  
Deux variables d'environnement sont à définir pour réaliser la compilation conditionnel (`#ifdef`) des différentes libraries : `SWITCH` et `LINUX`. (c.f. l'Initialisation du projet)  

### Visuel de l'interface disponible sur switch : 

**Menu principal :**   
<img src="./screenshots/menu_principal.png" width="600"/>  

**Ecran des matchs d'une journée de ligue simulé :**     
<img src="./screenshots/menu_matchs.png" width="600"/>

**Information sur un match :**       
<img src="./screenshots/match.png" width="600"/>

**Classement de la ligue (pour une journée sélectionnée) avec le 1er graphe :**       
<img src="./screenshots/classement.png" width="600"/>

**2nd graphe permettant de voir l'évolution des équipes au cours des journées :**       
<img src="./screenshots/graphique1.png" width="600"/>

## Comment lancer le projet

Liste des dépendances commune aux deux architectures: cmake, make, gcc, json-c, libcurl, sdl2, zlib  
  
Liste des dépendances pour la cross-compilation Nintendo switch: [DevKitPro, switch-dev](https://devkitpro.org/wiki/Getting_Started)

Initialisation du projet :
```
git clone https://gitlab.utc.fr/khimayan/ai30-lvgl.git
cd ai30-lvgl/
mkdir build
cmake -DCMAKE_BUILD_TYPE=Debug -DLINUX=ON -DSWITCH=OFF -G "CodeBlocks - Unix Makefiles" -S . -B ./build/
mkdir build_switch
/opt/devkitpro/portlibs/switch/bin/aarch64-none-elf-cmake -DCMAKE_BUILD_TYPE=Release -DLINUX=OFF -DSWITCH=ON -G "CodeBlocks - Unix Makefiles" -S . -B ./build_switch/
``` 


Pour compiler sur système UNIX : 
```shell
cmake --build ./build/ --target AI30
```
Pour compiler sur HorizonOS (Nintendo Switch) : 
```shell
cmake --build ./build_switch/ --target AI30_nro
```

Pour lancer sur système UNIX : 
```shell
./build/src/AI30
```
Pour téléverser et lancer sur HorizonOS (Nintendo Switch) : 
```shell
nxlink ./build_switch/src/AI30.nro -s
```

## Avantages, Inconvénients & Evolutions

### Avantages
- La projet réagit très bien à l'ajout/retrait d'agents. On peut très bien ajouter de nouveaux joueurs ou de nouvelles équipes, tout continuera de fonctionner.
- Simulation des ligues proche de la réalité, classement final trés cohérents.
- Interface client sur Nintendo Switch.
- Communication RESTFUL

### Inconvénients
- Les entraîneurs sont limités. On ne dispose d'entraîneurs que pour la Ligue 1.
- Toutes les ligues ne sont pas representées.
- La position des joueurs n'était pas renseigné sur la base de données FIFA 23. Nous avons donc du la déduire en fonction des statistiques des joueurs. Hors, pour certains joueurs (très peu, mais quand même), cette déduction s'est mal réalisée. Il existe certains milieux assez offensifs qui ont été considérés Attaquants (ou vice versa).

### Evolutions 

- Plus de communication inter-agents pourrait intervenir (exemple : ajout d'un arbitre pour communiquer avec les joueurs et les matches).
- La simulation peut encore etre ameliorer, en effet toutes les statistiques des joueurs ne sont pas prisent en compte. De plus nous n'avont pas de système de forme d'équipe...
- Pas d'interaction avec l'utilisateur ayant un impact sur la simulation (exemple : changement de stratégie...).
- Structure peut-etre un peu a revoir notamment au niveau de l'imbrication des structures.
- Ajout d'un affichage coté client pour les joueurs.
- Mise en place de l'évolution des joueurs.

