package agentServer

import (
	"fmt"
	"log"
	"time"

	g "football-manager/agent/game"
	t "football-manager/agent/team"
	i "football-manager/init"
)

func StartServer() {
	const url1 = ":8080"
	const url2 = "http://localhost:8080"
	
	g.InitTabOfRandomFloat()

	players, trainers, teams, leagues := i.Initialisation()
	fmt.Println(teams[0])
	fmt.Println(t.GetTeamOverall(teams[0]), " ", teams[0].TrainerName, " ", teams[0].Strategy )
	
	

	servAgt := NewRestServerAgent(url1, leagues, teams, players, trainers)

	log.Println("démarrage du serveur...")
	go servAgt.Start()
	//var a string 
	/*
	for _, league := range leagues {
		if league.Id == 7 {
			for _, mDay := range league.Calendar {
				for _, game := range mDay.Games {
					var wg sync.WaitGroup
					wg.Add(1)
					go g.PlayGame(game, &wg)
					wg.Wait()
				}
				g.RecalculDayRanking(mDay)
				g.PrintRanking(mDay.CurrentRanking)
			}
			l.PrintLeague(league)
		}
	}*/



	time.Sleep(time.Minute)
}
