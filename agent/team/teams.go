package team

import (
	"errors"
	"fmt"
	t "football-manager/agent"
	trainer "football-manager/agent/trainer"
	"strconv"
	"sync"
)

// Retourne 1 si la team1 est meilleure, 2 si la team2 est meilleure, 0 si teams équivalentes
func GetBestTeam(team1 *t.Team, team2 *t.Team) int {
	t1Overall := team1.Overall
	t2Overall := team2.Overall

	if t1Overall > t2Overall {
		return 1
	} else if t1Overall < t2Overall {
		return 2
	}

	return 0
}

func GetTeamOverall(team *t.Team) int{

	sumT := 0
	var titularplayers []*t.Player
	if team.Strategy != "" {
		titularplayers, _ = GetTitularPlayers(team, team.Strategy)
	} else {
		titularplayers, _ = GetTitularPlayers(team, "4-3-3")
	}
	team.Titulars = titularplayers

	for _, p := range titularplayers {
		//fmt.Printf("%s : %d\n", p.Name, p.Overall)
		sumT += p.Overall
	}


	sumS := 0
	var subsituteplayers []*t.Player
	if team.Strategy != "" {
		subsituteplayers = GetSubsitutePlayers(team, team.Strategy)
	} else {
		subsituteplayers = GetSubsitutePlayers(team, "4-3-3")
	}
	team.Subsitutes = subsituteplayers

	for _, p := range subsituteplayers {
		//fmt.Printf("%s : %d\n", p.Name, p.Overall)
		sumS += p.Overall
	}

	if len(titularplayers) != 0 && len(subsituteplayers) != 0 {
		//fmt.Printf("t : %d   s : %d\n", sumT/len(titularplayers), sumS/len(subsituteplayers))
		team.Overall = (sumT/len(titularplayers) + sumS/len(subsituteplayers)) / 2
	}

	return team.Overall
}

func existsTeam(joueur *t.Player, teams []*t.Team) (bool, *t.Team) {
	for _, team := range teams {
		if joueur.Club == team.ClubName {
			return true, team
		}
	}
	return false, new(t.Team)
}

func GetTitularPlayers(team *t.Team, formation string) ([]*t.Player, error) {

	nbDefenseursFormation, _ := strconv.Atoi(formation[0:1])
	nbMilieuxFormation, _ := strconv.Atoi(formation[2:3])
	nbAttaquantsFormation, _ := strconv.Atoi(formation[4:5])

	if nbDefenseursFormation+nbAttaquantsFormation+nbMilieuxFormation != 10 {
		fmt.Println("pas assez de joueurs", nbDefenseursFormation+nbAttaquantsFormation+nbMilieuxFormation)
		return nil, errors.New("cette formation ne contient pas 11 joueurs")
	}

	var Eleven []*t.Player

	var Attaquants []*t.Player
	var Defenseurs []*t.Player
	var Milieux []*t.Player
	var Gardiens []*t.Player

	nbAttaquantsDisponibles := 0
	nbDefenseursDisponibles := 0
	nbMilieuxDisponibles := 0
	nbGardiensDisponibles := 0

	squad := team.Players

	//fmt.Println(squad)

	for i := range squad {
		if squad[i].Position == "Offense" {
			nbAttaquantsDisponibles += 1
			Attaquants = append(Attaquants, squad[i])
			//fmt.Println(Attaquants)
		} else if squad[i].Position == "Middle" {
			nbMilieuxDisponibles += 1
			Milieux = append(Milieux, squad[i])
		} else if squad[i].Position == "Defense" {
			nbDefenseursDisponibles += 1
			Defenseurs = append(Defenseurs, squad[i])
		} else if squad[i].Position == "Goalkeeper" {
			nbGardiensDisponibles += 1
			Gardiens = append(Gardiens, squad[i])
		}
	}

	if nbDefenseursDisponibles < nbDefenseursFormation ||
		nbAttaquantsDisponibles < nbAttaquantsFormation ||
		nbMilieuxDisponibles < nbMilieuxFormation ||
		nbGardiensDisponibles == 0 {
		return nil, errors.New("pas assez de joueurs pour créer cette formation. Veuillez en choisir une autre")
	}

	Eleven = append(Eleven, Attaquants[0:nbAttaquantsFormation]...)
	Eleven = append(Eleven, Defenseurs[0:nbDefenseursFormation]...)
	Eleven = append(Eleven, Milieux[0:nbMilieuxFormation]...)
	Eleven = append(Eleven, Gardiens[0:1]...)

	return Eleven, nil

}

func GetSubsitutePlayers(team *t.Team, formation string) (subsitutes []*t.Player){
	titulars := team.Titulars

	for _, p := range team.Players {
		ti := true
		for _, titularP := range titulars {
			if p.Id == titularP.Id {
				ti = false
				break
			}
		}
		if ti {
			subsitutes = append(subsitutes, p)
		}
	}
	return subsitutes
}

func PlayersOfTheTeam(team t.Team, players []t.Player) []t.Player {
	var POTT []t.Player
	for i := range players {
		if team.ClubName == players[i].Club {
			POTT = append(POTT, players[i])
		}
	}

	return POTT
}

func CreateTeams(joueurs []*t.Player, trainers []*t.Trainer) []*t.Team {

	var teams []*t.Team
	var otherPlayers []*t.Player

	for i, joueur := range joueurs {
		exists, _ := existsTeam(joueur, teams)
		if !exists {
			// on crée une nouvelle équipe
			t := new(t.Team)

			// l'équipe prend le nom du club du joueur, et son premier joueur est le joueur lui-même
			t.Id = i + 1 // pour ne pas avoir d'id 0
			t.ClubName = joueur.Club
			t.Players = append(t.Players, joueur)

			// on ajoute l'équipe à la liste
			teams = append(teams, t)
		} else if exists {
			// on récupère tous les joueurs qui n'ont pas encore été mis dans une équipe
			otherPlayers = append(otherPlayers, joueur)
		}
	}

	// les autres joueurs sont insérés dans les équipes existantes
	for i := range teams {
		for _, joueur := range otherPlayers {
			if teams[i].ClubName == joueur.Club {
				teams[i].Players = append(teams[i].Players, joueur)
			}
		}
	}

	var wg sync.WaitGroup
	for _, team := range teams {
		wg.Add(1)
		go affectTrainerAndStrategy(team, trainers, &wg)
	}
	wg.Wait()

	return teams
}

func GetGoalTeam(team *t.Team) *t.Player {
	for  _, p := range team.Players {
		if p.Position == "Goalkeeper" {
			return p
		}
	}

	return nil
}

func affectTrainerAndStrategy(team *t.Team, trainers []*t.Trainer, wg *sync.WaitGroup) {
	defer wg.Done()
	var trainerOfTeam *t.Trainer

	for _, trainer := range trainers {
		if team.ClubName == trainer.Team {
			team.TrainerName = trainer.Name
			trainerOfTeam = trainer
		}
	}

	if trainerOfTeam != nil {
		var wg2 sync.WaitGroup
		wg2.Add(1)
		go ReceiveStrategy(team, trainerOfTeam, &wg2)
		wg2.Wait()
	}

	GetTeamOverall(team)
}

func getTrainer(team *t.Team, trainers []*t.Trainer) *t.Trainer {
	for _, trainer := range trainers {
		if team.TrainerName == trainer.Name {
			return trainer
		}
	}

	return nil
}

func ReceiveStrategy(team *t.Team, coach *t.Trainer, wg2 *sync.WaitGroup){
	defer wg2.Done()
	c := make(chan string) 

	team.TrainerChan = c
	coach.TrainerChan = c

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		
		team.Strategy = <- team.TrainerChan
		fmt.Println(team.Strategy)

	}()

	wg.Add(1)
	go trainer.SendStrategy(coach, &wg)
	wg.Wait()

	close(c)
}
