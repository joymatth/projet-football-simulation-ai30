package game

import (
	"fmt"
	t "football-manager/agent"
	pla "football-manager/agent/player"
	team "football-manager/agent/team"
	"math/rand"
	"sync"
	"time"
)

var tabOfRandomFloat []float64
var nGame int = 0

func InitTabOfRandomFloat(){
	rand.Seed(time.Now().UnixNano())
	tabOfRandomFloat = make([]float64, 10000000)
	for i := 0; i < 10000000; i++ {
        tabOfRandomFloat[i] = rand.Float64()
    }
}

func NewGame(id string, homeTeam *t.Team, awayTeam *t.Team) *t.Game {
	nGame += 500
	return &t.Game{Id: id, HomeTeam: homeTeam, AwayTeam: awayTeam, RandTab: tabOfRandomFloat[nGame:nGame+500]}
}

// PlayGame simulates a soccer game and determines the final result based on the players' actions and card chances
func PlayGame(g *t.Game) {
	// Create a wait group to track the goroutines
	var wg sync.WaitGroup

	// Launch a goroutine for each player to simulate their actions
	for _, p := range g.HomeTeam.Titulars {
		var wgPlayer sync.WaitGroup
		p.ChannelRec = make(chan float64)

		nActions := getNumBersOfActions(p, g)
		fmt.Println("Joueur ", p.Name, nActions["shot"], p.Position, p.Offensif)
		for i := 1; i < nActions["shot"]; i++ {

			getHomePlayerActions(&wgPlayer, p, g, "shot")

		}

		/*wgPlayer.Add(1)
		go getHomePlayerActions(&wgPlayer, &p, g, "pass")
		wgPlayer.Wait()*/

		getHomePlayerCard(&wgPlayer, p, g, "yellow")

		getHomePlayerCard(&wgPlayer, p, g, "red")
	}

	for _, p := range g.AwayTeam.Titulars {
		var wgPlayer sync.WaitGroup

		/*wgPlayer.Add(1)
		go getAwayPlayerActions(&wgPlayer, &p, g, "pass")
		wgPlayer.Wait()*/

		p.ChannelRec = make(chan float64)
		nActions := getNumBersOfActions(p, g)
		fmt.Println("Joueur ", p.Name, nActions["shot"], p.Position, p.Offensif)
		for i := 0; i < nActions["shot"]; i++ {
			
			getAwayPlayerActions(&wgPlayer, p, g, "shot")
		
		}

		getAwayPlayerCard(&wgPlayer, p, g, "yellow")


		
		getAwayPlayerCard(&wgPlayer, p, g, "red")

	}

	// Wait for all goroutines to finish
	wg.Wait()

	if g.HomeScore > g.AwayScore {
		g.Result = g.HomeTeam.ClubName
		g.HomeTeam.Points += 3
	} else if g.HomeScore < g.AwayScore {
		g.Result = g.AwayTeam.ClubName
		g.AwayTeam.Points += 3
	} else {
		g.Result = "Draw"
		g.HomeTeam.Points++
		g.AwayTeam.Points++
	}

	fmt.Println("match joué ", g.HomeTeam.ClubName, "(", g.HomeTeam.Overall , ") VS " ,g.AwayTeam.ClubName, "(", g.AwayTeam.Overall, ")","==> ", g.HomeScore, " : ", g.AwayScore)
}

// CALCUL PAR RAPPORT A LA DIFFERENCE ENTRE LES DEUX EQUIPES
func getHomePlayerActions(wg *sync.WaitGroup, p *t.Player, g *t.Game, t string) {
	var playerShotSuccess float64
	var playerPassSuccess float64
	//var playerStopSuccess float64

	var wgPlayer sync.WaitGroup
	wgPlayer.Add(1)
	go func() {
		defer wgPlayer.Done()
		

		if t == "shot" {
			playerShotSuccess = <-p.ChannelRec
			g.HomeLogs += fmt.Sprintf("Tir de %s\n", p.Name)
			if g.RandTab[g.NAction] < playerShotSuccess + 0.20{
				g.NAction++
				goal := team.GetGoalTeam(g.AwayTeam)
				chanceToStop := (float64(goal.Reflexe+goal.Plongeon+goal.JeuMain+goal.JeuPied)/4.)/100.+g.RandTab[g.NAction]
				g.NAction++

				if chanceToStop < playerShotSuccess + g.RandTab[g.NAction] {
					fmt.Println("But : ", chanceToStop, " ", playerShotSuccess)
					g.HomeLogs += fmt.Sprintf("But de %s\n", p.Name)
					g.HomeScore++

				} else {
					fmt.Println("Arret : ", chanceToStop, " ", playerShotSuccess)
					g.HomeLogs += fmt.Sprintf("Arret de %s\n", goal.Name)
				}
				g.NAction++
			}
		} else if t == "pass" {
			playerPassSuccess = <-p.ChannelRec
			if pla.Random() < playerPassSuccess {

			}
			/*
				} else if t == "stop" {
					playerStopSuccess = <-p.ChannelRec
					if pla.Random() > playerStopSuccess {
						g.homeScore++
					}*/
		}
	}()

	wgPlayer.Add(1)
	go pla.SimulateAction(p, t, &wgPlayer)
	wgPlayer.Wait()
}

func getHomePlayerCard(wg *sync.WaitGroup, p *t.Player, g *t.Game, ty string) {
	var playeryellowCardChance float64
	var playerredCardChance float64
	var wgPlayer sync.WaitGroup
	wgPlayer.Add(1)

	go func() {
		defer wgPlayer.Done()
		if ty == "yellow" {
			playeryellowCardChance = <-p.ChannelRec
			if pla.Random() < playeryellowCardChance {
				g.HomeLogs += fmt.Sprintf("Carton Jaune pour %s\n", p.Name)
				g.HomeYellowCards++
			}
		} else if ty == "red" {
			playerredCardChance = <-p.ChannelRec
			if pla.Random() < playerredCardChance {
				g.HomeLogs += fmt.Sprintf("Carton rouge pour %s\n", p.Name)
				g.HomeRedCards++
			}
		}
	}()

	wgPlayer.Add(1)
	go pla.SimulateCardChance(p, ty, &wgPlayer)
	wgPlayer.Wait()
}

func getAwayPlayerActions(wg *sync.WaitGroup, p *t.Player, g *t.Game, ty string) {

	var playerShotSuccess float64
	var playerPassSuccess float64
	var wgPlayer sync.WaitGroup
	
	wgPlayer.Add(1)
	go func() {

		defer wgPlayer.Done()
		if ty == "shot" {
			playerShotSuccess = <-p.ChannelRec 
			g.AwayLogs += fmt.Sprintf("Tir de %s\n", p.Name)

			if g.RandTab[g.NAction] < playerShotSuccess + 0.20 {
				g.NAction++
				goal := team.GetGoalTeam(g.HomeTeam)
				chanceToStop := (float64(goal.Reflexe+goal.Plongeon+goal.JeuMain+goal.JeuPied)/4.)/100.+g.RandTab[g.NAction]
				g.NAction++

				if chanceToStop < playerShotSuccess + g.RandTab[g.NAction]{
					fmt.Println("But : ", chanceToStop, " ", playerShotSuccess)
					g.AwayLogs += fmt.Sprintf("But de %s\n", p.Name)
					g.AwayScore++

				} else {
					fmt.Println("Arret : ", chanceToStop, " ", playerShotSuccess)
					g.AwayLogs += fmt.Sprintf("Arret de %s\n", goal.Name)
				}
				g.NAction++
			}
		} else if ty == "pass" {
			playerPassSuccess = <-p.ChannelRec
			if pla.Random() < playerPassSuccess {

			}
		}
	}()

	wgPlayer.Add(1)
	go pla.SimulateAction(p, ty, &wgPlayer)
	wgPlayer.Wait()
}

func getAwayPlayerCard(wg *sync.WaitGroup, p *t.Player, g *t.Game, ty string) {

	var playeryellowCardChance float64
	var playerredCardChance float64
	var wgPlayer sync.WaitGroup
	wgPlayer.Add(1)

	go func() {
		defer wgPlayer.Done()
		if ty == "yellow" {
			playeryellowCardChance = <-p.ChannelRec
			if pla.Random() < playeryellowCardChance {
				g.AwayLogs += fmt.Sprintf("Carton Jaune pour %s\n", p.Name)
				g.AwayYellowCards++
			}
		} else if ty == "red" {
			playerredCardChance = <-p.ChannelRec
			if pla.Random() < playerredCardChance {
				g.AwayLogs += fmt.Sprintf("Carton Rouge pour %s\n", p.Name)
				g.AwayRedCards++
			}
		}
	}()

	wgPlayer.Add(1)
	go pla.SimulateCardChance(p, ty, &wgPlayer)
	wgPlayer.Wait()
}

func getNumBersOfActions(p *t.Player, g *t.Game) map[string]int {

	nAction := make(map[string]int)

	homeTeam := g.HomeTeam
	awayTeam := g.AwayTeam

	overallDiff := homeTeam.Overall - awayTeam.Overall

	if p.Club == homeTeam.ClubName {
		if p.Offensif == "High" && p.Position == "Offense" {
			if overallDiff <= 2 {
				nAction["shot"] = rand.Intn(3)
			} else if overallDiff <= 4 {
				nAction["shot"] = rand.Intn(4)
			} else if overallDiff <= 6 {
				nAction["shot"] = rand.Intn(6)
			} else {
				nAction["shot"] = rand.Intn(6 - 2) + 2
			}
		} else if (p.Offensif == "Medium" || p.Offensif == "High") && p.Position != "Goalkeeper" {
			if overallDiff <= 2 {
				nAction["shot"] = rand.Intn(2)
			} else if overallDiff <= 4 {
				nAction["shot"] = rand.Intn(3)
			} else if overallDiff <= 6 {
				nAction["shot"] = rand.Intn(3 - 1) + 1
			} else {
				nAction["shot"] = rand.Intn(4 - 1) + 1
			}
		} else {
			if rand.Intn(100) == 50 {
				nAction["shot"] = 1
			} else {
				fmt.Println("ici")
				nAction["shot"] = 0
			}
		}

		if p.Defensif == "High" {
			nAction["yellowCard"] = rand.Intn(2)
		} else if p.Defensif == "Medium" && p.Agressivity > 70 {
			nAction["yellowCard"] = rand.Intn(2)
		} else {
			nAction["yellowCard"] = rand.Intn(1)
		}

		if p.Defensif == "High" {
			nAction["redCard"] = rand.Intn(1)
		} else if p.Defensif == "Medium" {
			nAction["redCard"] = rand.Intn(1)
		} else {
			nAction["redCard"] = 0
		}

	} else if p.Club == awayTeam.ClubName {
		if p.Offensif == "High" && p.Position == "Offense" {
			if -overallDiff <= 2 {
				nAction["shot"] = rand.Intn(2)
			} else if -overallDiff <= 4 {
				nAction["shot"] = rand.Intn(3)
			} else if -overallDiff <= 6 {
				nAction["shot"] = rand.Intn(6)
			} else if -overallDiff > 6 {
				nAction["shot"] = rand.Intn(6 - 3) + 3
			}
		} else if (p.Offensif == "Medium" || p.Offensif == "High") && p.Position != "Goalkeeper" {
			if -overallDiff <= 2 {
				nAction["shot"] = rand.Intn(2)
			} else if -overallDiff <= 4 {
				nAction["shot"] = rand.Intn(3)
			} else if -overallDiff <= 6 {
				nAction["shot"] = rand.Intn(3 - 1) + 1
			} else if -overallDiff > 6{
				nAction["shot"] = rand.Intn(4 - 1) + 1
			}
		} else {
			if rand.Intn(100) == 50 {
				nAction["shot"] = 1
			} else {
				nAction["shot"] = 0
			}
		}

		if p.Defensif == "High" {
			nAction["yellowCard"] = rand.Intn(2)
		} else if p.Defensif == "Medium" && p.Agressivity > 70 {
			nAction["yellowCard"] = rand.Intn(2)
		} else {
			nAction["yellowCard"] = rand.Intn(1)
		}

		if p.Defensif == "High" {
			nAction["redCard"] = rand.Intn(1)
		} else if p.Defensif == "Medium" {
			nAction["redCard"] = rand.Intn(1)
		} else {
			nAction["redCard"] = 0
		}
	}

	return nAction
}