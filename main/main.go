package main

import (
	as "football-manager/agent/agentServer"
)

/*
import (
	"fmt"
	t "football-manager/agent"
	"football-manager/agent/game"
	i "football-manager/init"
	"sync"
)*/

func main() {

	/*
	// Read the JSON file
	_, teams, _ := i.Initialisation()

	g := t.Game{
		HomeTeam: teams[0],
		AwayTeam: teams[1],
	}

	fmt.Printf("%s VS %s \n", g.HomeTeam.ClubName, g.AwayTeam.ClubName)

	// Simulate the game and determine the result
	var wg sync.WaitGroup
	wg.Add(1)
	go game.PlayGame(&g, &wg)
	wg.Wait()
	// Print the result
	fmt.Println(g.Result)
	*/

	as.StartServer()
}
