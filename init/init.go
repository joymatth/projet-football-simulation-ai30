package init

import (
	"fmt"
	t "football-manager/agent"

	l "football-manager/agent/league"
	p "football-manager/agent/player"
	team "football-manager/agent/team"
	trainer "football-manager/agent/trainer"
)

func Initialisation() (players []*t.Player, trainers []*t.Trainer, teams []*t.Team, leagues []*t.League) {
	players = p.PlayersJSON() // renvoie un tableau avec toutes les équipes
	trainers = trainer.CreateTrainers()
	teams = team.CreateTeams(players, trainers)   // renvoie un tableau avec toutes les équipes
	leagues = l.CreateLeagues(teams) // renvoie un tableau avec toutes les ligues
	//l.FillLeagues(leagues, teams)

	/* Affichage des équipes selon leur championnat*/
	for _, league  := range leagues {
		league = l.CreateCalendar(league)
		//fmt.Printf("\n================== Championnat : %s (ID=%d) (Nombre d'équipes : %d)==================\n", league.Name, league.Id, len(league.Teams))
			/*for _, team := range league.Teams {
				fmt.Println("ID n°", team.Id, " - ", team.ClubName)
				for _, player := range team.Players {
						fmt.Println("ID n°", player.Id, "Nom : ", player.Name, " - Club : ", player.Club, "(", player.Overall, ")")
				}
			}*/
/*
			for _, matchDay := range league.Calendar {
				g.PrintMatchDay(matchDay)
			}*/
	}

	/* Affichage des joueurs ayant + de 85 de général*/
	fmt.Printf("\n\n================== Meilleurs joueurs ==================\n")
	for _, player := range players {
		if player.Overall >= 90 {
			fmt.Println("ID n°", player.Id, "Nom : ", player.Name, " - Club : ", player.Club, "(", player.Overall, ")")
		}
	}

	fmt.Printf("\n\n==================Entraineur ==================\n")
	for _, trainer := range trainers {
		fmt.Println("Nom : ", trainer.Name, " - Strategy : ", trainer.Strategy, " - Team : ", trainer.Team)

	}

	return players, trainers, teams, leagues
}
