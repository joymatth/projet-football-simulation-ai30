package trainer

import (
	"encoding/json"
	"fmt"
	t "football-manager/agent"
	"io/ioutil"
	"sync"
)

// créer un fichier trainers.go et ajouter cette fonction
func CreateTrainers() []*t.Trainer {
	content, err := ioutil.ReadFile("bdd_coachs.json")
	if err != nil {
		fmt.Println(err.Error())
	}

	// unmarshal
	var Trainers []*t.Trainer
	json.Unmarshal(content, &Trainers)

	// création d'un ID unique
	for i := range Trainers {
		Trainers[i].Id = i + 1
	}

	return Trainers
}

func SendStrategy(trainer *t.Trainer, wg *sync.WaitGroup) {
	defer wg.Done()
	
	trainer.TrainerChan <- trainer.Strategy

}