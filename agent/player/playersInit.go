package player

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	t "football-manager/agent"
)

func PlayerPosition(joueur *t.Player) {
	aptDef := (joueur.DefConsience + joueur.Interception + joueur.TacleDebout + joueur.TacleGlisse) / 4
	aptAtt := (joueur.Finishing + joueur.Volee) / 2
	aptGK := (joueur.Reflexe + joueur.Plongeon + joueur.JeuMain + joueur.JeuPied) / 4

	var position string

	if aptGK > aptDef || aptGK > aptAtt {
		position = "Goalkeeper"
	} else if (aptAtt > aptDef && joueur.Offensif == "High" && joueur.Defensif == "Low") || (aptAtt > aptDef && (aptAtt-aptDef) > 35) {
		position = "Offense"
	} else if (aptDef > aptAtt && (aptDef-aptAtt) > 10) || (aptDef > aptAtt && joueur.Defensif == "High" && joueur.Offensif == "Low") {
		position = "Defense"
	} else {
		position = "Middle"
	}

	joueur.Position = position

}

func PlayersJSON() []*t.Player {
	content, err := ioutil.ReadFile("bdd_joueurs.json")
	if err != nil {
		fmt.Println(err.Error())
	}

	// unmarshal
	var Players []*t.Player
	json.Unmarshal(content, &Players)

	// création d'un ID unique + calcul du poste
	for i := range Players {
		Players[i].Id = i + 1
		PlayerPosition(Players[i])
	}

	return Players
}

func GetPlayers(pId int, players []*t.Player) *t.Player {
	for _, p := range players {
		if p.Id == pId {
			return p
		}
	}

	return nil
}
