package player

import (
	"math/rand"
	"sync"
	"time"

	t "football-manager/agent"
)

func GetPlayer(id int, PlayersInfos []t.Player) *t.Player {
	for _, info := range PlayersInfos {
		if info.Id == id {
			return &info
		}
	}
	return nil
}

// simulateAction simulates a player's action (shot or pass) and sends the success rate to the given channel
func SimulateAction(p *t.Player, action string, wg *sync.WaitGroup) {
	defer wg.Done()
	switch action {
	case "shot":
		p.ChannelRec <- (float64(p.Shooting) / 100. + float64(p.Finishing) / 100. + float64(p.Skills) / 100.) / 3.
	case "pass":
		p.ChannelRec <- (float64(p.Passing) / 100. * float64(p.Vista) / 100. * float64(p.Stamina) / 100.) / 2.
	default:
		p.ChannelRec <- 0
	}
}

// simulateCardChance simulates a player's chance of receiving a yellow or red card and sends the result to the given channel
func SimulateCardChance(p *t.Player, cardType string, wg *sync.WaitGroup) {
	defer wg.Done()
	switch cardType {
	case "yellow":
		p.ChannelRec <- (float64(p.TacleDebout) / 100. * float64(p.TacleGlisse) / 100.) / 2.
	case "red":
		p.ChannelRec <- float64(p.YellowCards) / 100. / 2.
	default:
		p.ChannelRec <- 0
	}
}

// random generates a rndom float64 between 0 and 1
func Random() float64 {
	rand.Seed(time.Now().UnixNano())
    	//generator := rand.New(source)
	randomF := rand.Float64()
	return randomF
}
